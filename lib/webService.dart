import 'package:dio/dio.dart';
import 'package:ecommerce/user/userModel.dart';

class webService {
  final dio = Dio(BaseOptions(
    baseUrl: 'http://bootcamp.cyralearnings.com/',
    contentType: 'application/json',
  ));

  Future<UserModel?> login(String username, String password) async {
    try {
      print('$username, $password');
      final response = await dio.post('login.php', data: {
        'username': username,
        'password': password,
      });
      print('response: $response');
      if (response.statusCode == 200) {
        final Map<String, dynamic> responseData = response.data;

        final UserModel user = UserModel.fromJson(responseData);
        print('parsed: $user');
        return user;
      }
    } catch (e) {
      if (e is DioException) {
        if (e.response != null) {
          Map<String, dynamic> responseData = e.response!.data;
          String error = responseData['error'] ?? 'An error occurred';
          print('error is: ${error}');

          return UserModel.fromJson(responseData);
        } else {
          print('Dio error: ${e.message}');
        }
      }
    }
  }

  Future<UserModel?> register(Map data) async {
    try {
      print('data: ${data}');
      final response = await dio.post('registration.php', data: data);
      print('response: $response');
      if (response.statusCode == 200) {
        final Map<String, dynamic> responseData = response.data;

        final UserModel user = UserModel.fromJson(responseData);
        print('parsed: $user');
        return user;
      }
    } catch (e) {
      if (e is DioException) {
        if (e.response != null) {
          Map<String, dynamic> responseData = e.response!.data;
          String error = responseData['error'] ?? 'An error occurred';
          print('error is: ${error}');

          return UserModel.fromJson(responseData);
        } else {
          print('Dio error: ${e.message}');
        }
      }
    }
  }
}
