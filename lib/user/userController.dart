import 'package:ecommerce/user/userModel.dart';
import 'package:ecommerce/webService.dart';
import 'package:get/get.dart';

class UserController extends GetxController {
  RxBool isLoading = false.obs;

  Future<void> register(data) async {
    print('data: $data');
    final response = await webService().register(data);
    print('result in register controller');
  }

  Future<UserModel?> login(String username, String password) async {
    final result = await webService().login(username, password);
    print('result in login controller $result');
    return result;
  }
}
