class UserModel {
  final String? username;
  final String? name;
  final String? address;
  final String? phone;
  final String? password;

  UserModel({
    required this.username,
    this.password,
    this.address,
    this.name,
    this.phone,
  });

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
        username: json['username'] as String?,
        name: json['name'] as String?,
        address: json['address'] as String?,
        phone: json['phone'] as String?,
      );

  Map<String, dynamic> toJson() => {
        "username": username,
        "name": name,
        "phone": phone,
        "address": address,
        "password": password,
      };
}
